const { TIMEOUT } = require("dns");
const fs = require("fs");
const path = require("path");

function callBackToReadAfile() {
  fs.readFile("./lipsum.txt", "utf8", function (err, data) {
    if (err) {
      return console.error(err);
    } else {
      console.log("Base file read sucessfully");
    }
  });
}

function callBackToconvertTheContentOfFileIntoUppercase() {
  let myFileName = "myUpperCaseFile.txt";
  fs.readFile("./lipsum.txt", "utf8", function (err, data) {
    if (err) {
      return console.error(err);
    } else {
      console.log("Base file read sucessfully in upper function");
      let myData = data.toString().toUpperCase().split("\n\n");
      let arrayAfterTrim = myData.map((everyElement) => {
        return everyElement.trim();
      });
      let sentancesArray = arrayAfterTrim.join("\n").toString();

      fs.writeFile(myFileName, sentancesArray, "utf8", function (err, data) {
        if (err) {
          return console.error(err);
        } else {
          console.log("Upper file created sucessfully");
        }
      });

      let addUppername = () => {
        fs.writeFile(
          "filenames.txt",
          myFileName + "\n",
          "utf8",
          function (err, data) {
            if (err) {
              return console.error(err);
            } else {
              console.log("Upper file name added sucessfully to filenames.txt");
            }
          }
        );
      };
      setTimeout(addUppername, 1000);
    }
  });
}

function callBackToconvertTheContentOfFileIntoLowercase() {
  let myFileName = "myLowerCaseFile.txt";

  fs.readFile("./myUpperCaseFile.txt", function (err, data) {
    if (err) {
      return console.error(err);
    } else {
      console.log("Upper file read sucessfully in lower function");
      let mySplittedDataArray = data.toString().toLowerCase().split(". ");

      let arrayAfterTrim = mySplittedDataArray.map((everyElement) => {
        return everyElement.trim();
      });
      let sentancesArray = arrayAfterTrim.join("\n");
      let stringData = sentancesArray.toString();
      fs.writeFile(myFileName, stringData, "utf8", function (err) {
        if (err) {
          return console.error(err);
        } else {
          console.log("lower file added sucessfully");
        }
      });
      let addLowerFileName = () => {
        fs.appendFile(
          "filenames.txt",
          myFileName + "\n",
          "utf8",
          function (err, data) {
            if (err) {
              return console.error(err);
            } else {
              console.log("lower file name added sucessfully to filenames.txt");
            }
          }
        );
      };

      setTimeout(addLowerFileName, 2000);
    }
  });
}

function callBacktoReadMultipleFilesAndWriteThemToASingleFile() {
  let myFileName = "mySortedFile.txt";
  fs.readFile("./myUpperCaseFile.txt", function (err, data) {
    if (err) {
      return console.error(err);
    } else {
      console.log("Upper file read sucessfully in sorted function");
      fs.writeFile(myFileName, data + "\n", "utf8", function (err) {
        if (err) {
          return console.error(err);
        } else {
          console.log("Content of upper file added sucessfully to sorted file");
        }
      });
    }
  });
  let addlowerDataToSortedFile = () => {
    fs.readFile("./myLowerCaseFile.txt", function (err, data) {
      if (err) {
        return console.error(err);
      } else {
        console.log("Lower file read sucessfully in sorted function");
        fs.appendFile(myFileName, data, "utf8", function (err) {
          if (err) {
            return console.error(err);
          } else {
            console.log(
              "Content of lower file added sucessfully to sorted file"
            );
          }
        });
      }
    });
  };

  setTimeout(addlowerDataToSortedFile, 0000);
  let readDataForSort = () => {
    fs.readFile(myFileName, function (err, data) {
      if (err) {
        return console.error(err);
      } else {
        console.log("sortedfile read sucessfully in sorted function");
        let stringData = data.toString().trim().split("\n").sort().join("\n");
        fs.writeFile(myFileName, stringData, function (err) {
          if (err) {
            return console.error(err);
          } else {
            console.log("Sorted content added sucessfully");
          }
        });
      }
    });
  };

  setTimeout(readDataForSort, 6000);
  let addSortedFilenameTofileNames = () => {
    fs.appendFile(
      "filenames.txt",
      myFileName + "\n",
      "utf8",
      function (err, data) {
        if (err) {
          return console.error(err);
        } else {
          console.log("Sorted File name added sucessfully to filenames.txt");
        }
      }
    );
  };

  setTimeout(addSortedFilenameTofileNames, 9000);
}

let deleteContentFilesInfileNames = () => {
  let readFiles = () => {
    fs.readFile("filenames.txt", (err, data) => {
      if (err) {
        return console.error(err);
      } else {
        console.log("Filenames.txt read sucessfully");
        let stringData = data.toString().trim().split("\n");

        let deleteFilesFromFileNames = () => {
          stringData.map((everyFile) => {
            fs.unlink(everyFile, function (err) {
              if (err) {
                throw err;
              } else {
                console.log(`${everyFile} is Deleted`);
              }
            });
          });
        };
        setTimeout(deleteFilesFromFileNames, 15000);
      }
    });
  };
  setTimeout(readFiles, 12000);
};

module.exports = {
  callBackToReadAfile: callBackToReadAfile,
  callBackToconvertTheContentOfFileIntoUppercase:
    callBackToconvertTheContentOfFileIntoUppercase,
  callBackToconvertTheContentOfFileIntoLowercase:
    callBackToconvertTheContentOfFileIntoLowercase,
  callBacktoReadMultipleFilesAndWriteThemToASingleFile:
    callBacktoReadMultipleFilesAndWriteThemToASingleFile,
  deleteContentFilesInfileNames: deleteContentFilesInfileNames,
};
