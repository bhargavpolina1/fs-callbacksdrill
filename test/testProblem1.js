let myCallBack = require("../problem1.js");
let myFilePath1 = "/home/phanipolina/Desktop/fs_callbacks/RandomJsFiles/1.json";
let myFilePath2 = "/home/phanipolina/Desktop/fs_callbacks/RandomJsFiles/2.json";
let myFilePath3 = "/home/phanipolina/Desktop/fs_callbacks/RandomJsFiles/3.json";
let myFilePath4 = "/home/phanipolina/Desktop/fs_callbacks/RandomJsFiles/4.json";
let myFilePath5 = "/home/phanipolina/Desktop/fs_callbacks/RandomJsFiles/5.json";
content = "Namsthe";

let callBackToCreateADirectoryFn = myCallBack.callBackToCreateADirectory;
let callBackToCreateJsonFilesFn = myCallBack.callBackToCreateJsonFiles;
let callBackToDeleteJsonFilesFn = myCallBack.callBackToDeleteJsonFiles;

function createDirectory(directoryName) {
  callBackToCreateADirectoryFn(directoryName);
}

function callBackToCreateJsonFiles(myFilePath, content) {
  callBackToCreateJsonFilesFn(myFilePath, content);
}
function callBackToDeleteJsonFiles(myFilePath) {
  callBackToDeleteJsonFilesFn(myFilePath);
}
createDirectory("RandomJsFiles");
callBackToCreateJsonFiles(myFilePath1, content);
callBackToCreateJsonFiles(myFilePath2, content);
callBackToCreateJsonFiles(myFilePath3, content);
callBackToCreateJsonFiles(myFilePath4, content);
callBackToCreateJsonFiles(myFilePath5, content);
callBackToDeleteJsonFiles(myFilePath1);
callBackToDeleteJsonFiles(myFilePath2);
callBackToDeleteJsonFiles(myFilePath3);
callBackToDeleteJsonFiles(myFilePath4);
callBackToDeleteJsonFiles(myFilePath5);
