let myCallBack = require("../problem2.js");
let callBackToReadAfile = myCallBack.callBackToReadAfile;
let callBackToconvertTheContentOfFileIntoUppercase =
  myCallBack.callBackToconvertTheContentOfFileIntoUppercase;
let callBackToconvertTheContentOfFileIntoLowercase =
  myCallBack.callBackToconvertTheContentOfFileIntoLowercase;
let callBacktoReadMultipleFilesAndWriteThemToASingleFile =
  myCallBack.callBacktoReadMultipleFilesAndWriteThemToASingleFile;
let callBacktoDeleteFiles = myCallBack.deleteContentFilesInfileNames;

function callBackToReadAfilefn() {
  callBackToReadAfile();
}
function callBackToconvertTheContentOfFileIntoUppercaseFn() {
  callBackToconvertTheContentOfFileIntoUppercase();
}
function callBackToconvertTheContentOfFileIntoLowercaseFn() {
  callBackToconvertTheContentOfFileIntoLowercase();
}

function callBacktoReadMultipleFilesAndWriteThemToASingleFileFn() {
  callBacktoReadMultipleFilesAndWriteThemToASingleFile();
}

function callBacktoDeleteFilesFn() {
  callBacktoDeleteFiles();
}
callBackToReadAfilefn();
setTimeout(callBackToconvertTheContentOfFileIntoUppercaseFn, 2000);
setTimeout(callBackToconvertTheContentOfFileIntoLowercaseFn, 4000);
setTimeout(callBacktoReadMultipleFilesAndWriteThemToASingleFileFn, 7000);
setTimeout(callBacktoDeleteFilesFn, 13000);
