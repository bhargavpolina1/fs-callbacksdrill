const fs = require("fs");
const path = require("path");

function callBackToCreateADirectory(directoryName) {
  fs.mkdir(path.join(__dirname, directoryName), (err) => {
    if (err) {
      return console.error(err);
    }
    console.log("Directory created successfully!");
  });
}

function callBackToCreateJsonFiles(myFilePath, content) {
  fs.writeFile(path.join(myFilePath, ""), content, "utf8", function (err) {
    if (err) {
      throw err;
    } else {
      console.log("File created!");
    }
  });
}

function callBackToDeleteJsonFiles(myFilePath) {
  let deleteFileFn = () => {
    fs.unlink(path.join(myFilePath, ""), function (err) {
      if (err) throw err;
      console.log("File deleted!");
    });
  };
  setTimeout(deleteFileFn, 3000);
}

module.exports = {
  callBackToCreateADirectory: callBackToCreateADirectory,
  callBackToCreateJsonFiles: callBackToCreateJsonFiles,
  callBackToDeleteJsonFiles: callBackToDeleteJsonFiles,
};
